public class Goverment extends Person{
    public String division;
    public String position;

    public Goverment(Integer id, String name, String gender, String address, Integer age, String division, String position) {
        super(id, name, gender, address, age);
        this.division = division;
        this.position = position;
    }

    @Override
    public String toString() {
        return "Goverment{" +
                "division='" + division + '\'' +
                ", position='" + position + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }
}
