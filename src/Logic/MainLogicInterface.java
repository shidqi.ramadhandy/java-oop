package Logic;

import Logic.Logic01.Logic01Soal02;
import Logic.logicInterface.LogicInterface;
import Logic.logicInterface.logic01Impl.*;
import Logic.logicInterface.logic02Impl.*;

public class MainLogicInterface {
    public static void main(String[] args) {
        //deklarasi basiclogic
        BasicLogic basicLogic = new BasicLogic(9);

        System.out.println("==============================Logic 01==============================");
        LogicInterface logic01Soal01Impl = new Logic01Soal01Impl(basicLogic);
        logic01Soal01Impl.cetakArray();
        LogicInterface logic01Soal01aImpl = new Logic01Soal01aImpl(basicLogic);
        System.out.println("\n");
        logic01Soal01aImpl.cetakArray();
        LogicInterface logic01Soal01bImpl = new Logic01Soal01bImpl(basicLogic);
        System.out.println("\n");
        logic01Soal01bImpl.cetakArray();
        LogicInterface logic01Soal01cImpl = new Logic01Soal01cImpl(basicLogic);
        System.out.println("\n");
        logic01Soal01cImpl.cetakArray();
        LogicInterface logic01Soal01dImpl = new Logic01Soal01dImpl(basicLogic);
        System.out.println("\n");
        logic01Soal01dImpl.cetakArray();
        System.out.println("\nSoal 01 ============================================================");
        //Soal2
        LogicInterface logic01Soal02Impl = new Logic01Soal02Impl(basicLogic);
        logic01Soal02Impl.cetakArray();
        LogicInterface logic01Soal02aImpl = new Logic01Soal02aImpl(basicLogic);
        System.out.println("\n");
        logic01Soal02aImpl.cetakArray();
        LogicInterface logic01Soal02bImpl = new Logic01Soal02bImpl(basicLogic);
        System.out.println("\n");
        logic01Soal02bImpl.cetakArray();
        LogicInterface logic01Soal02cImpl = new Logic01Soal02cImpl(basicLogic);
        System.out.println("\n");
        logic01Soal02cImpl.cetakArray();
        LogicInterface logic01Soal02dImpl = new Logic01Soal02dImpl(basicLogic);
        System.out.println("\n");
        logic01Soal02dImpl.cetakArray();
        System.out.println("\nSoal 02 ============================================================");


        System.out.println("\n\n==============================Logic 02==============================");
        LogicInterface logic02Soal01Impl = new Logic02Soal01Impl(basicLogic);
        logic02Soal01Impl.cetakArray();
    }
}
