package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal09 extends BasicLogic {
    public Logic01Soal09(int n) {
        super(n);
    }

    public void isiArray(){

        int k = 1;
        for (int i = 0; i < n; i++) {
            this.array[0][i] = String.valueOf(k);

            k = Integer.parseInt(this.array[0][i])*3;
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
