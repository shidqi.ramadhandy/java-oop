package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal06 extends BasicLogic {

    public Logic01Soal06(int n) {
        super(n);
    }

    public void isiArray(){
        int k = 1; //nilai awal
        int l =10; //nilai akhir
        int tampungN,batas = 0;
        for (int i=k; i<=l; i++){
            tampungN = 0;
            for (int j=1;j<=i;j++){

                if (i%j==0){
                    tampungN=tampungN+1;
                }
            }
            if (tampungN==2){
                this.array[0][batas] = String.valueOf(i);
                if (batas == n-1) break;
                else {
                    l = (int)(Math.pow(l, i));
                    batas++;
                }
            }
        }

        for(int i=0; i< this.n; i++){
            this.array[0][i] = this.array[0][i];

        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
