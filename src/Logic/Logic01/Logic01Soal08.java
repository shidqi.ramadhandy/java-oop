package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal08 extends BasicLogic {
    public Logic01Soal08(int n) {
        super(n);
    }

    public void isiArray(){

        char j = 'A';
        for ( int i=0; i<this.n; i++) {
            if (i %2 == 0){
                this.array[0][i] = String.valueOf(j);

                //String k = (this[i]);

            } else {
                this.array[0][i] = String.valueOf(i+1);
            }
            j++;
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
