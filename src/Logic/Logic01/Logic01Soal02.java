package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal02 extends BasicLogic {

    public Logic01Soal02(int n) {
        super(n);
    }

    public void isiArray(){

        int j = 1;
        int k = 1;
        for (int i = 0; i < this.n; i++) {
            if (i %2 == 0){
                this.array[0][i] = String.valueOf(j);
                j++;

            }else {

                this.array[0][i] = String.valueOf(k*3);
                k++;
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
