package Logic.logicInterface.logic01Impl;
import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal01Impl implements LogicInterface{

    private final BasicLogic logic;

    public Logic01Soal01Impl(BasicLogic logic){
        this.logic = logic;
    }

    public void isiArray(){

        int j = 1;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(j);
            j++;
        }

    }
    @Override
    public void cetakArray() {

        this.isiArray();
        this.logic.printSingle();
    }
}
