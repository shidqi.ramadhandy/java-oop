package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal03Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal03Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(i*2);
        }
    }

    @Override
    public void cetakArray() {

        this.isiArray();
        this.logic.printSingle();
    }
}
