package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal01dImpl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal01dImpl(BasicLogic logic) {
        this.logic = logic;
    }

    void isianArray(){
        for (int i = 0; i < this.logic.n; i++) {

            this.logic.array[0][i] = String.valueOf(i+1);

        }
    }

    @Override
    public void cetakArray() {

        this.isianArray();
        this.logic.printSingle();
    }
}
