package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal08Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal08Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        char j = 'A';
        for ( int i=0; i<this.logic.n; i++) {
            if (i %2 == 0){
                this.logic.array[0][i] = String.valueOf(j);

                //String k = (this[i]);

            } else {
                this.logic.array[0][i] = String.valueOf(i+1);
            }
            j++;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
