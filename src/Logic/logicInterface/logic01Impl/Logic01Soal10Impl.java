package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal10Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal10Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            double k = (double) (i);
            this.logic.array[0][i] = String.valueOf((int)Math.pow(k, 3));

        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
