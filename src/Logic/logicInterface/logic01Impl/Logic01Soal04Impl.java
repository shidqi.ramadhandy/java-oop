package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

import java.util.Base64;

public class Logic01Soal04Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal04Impl(BasicLogic logic) {
        this.logic = logic;
    }

    void isiArray(){
        for (int i = 0; i < this.logic.n; i++){
            if (i < 2) {
                this.logic.array[0][i] = String.valueOf(1);
            } else {
                this.logic.array[0][i] = String.valueOf(Integer.parseInt(this.logic.array[0][i - 1]) + Integer.parseInt(this.logic.array[0][i - 2]));
            }
        }
    }

    @Override
    public void cetakArray() {
    this.isiArray();
    this.logic.printSingle();
    }
}
