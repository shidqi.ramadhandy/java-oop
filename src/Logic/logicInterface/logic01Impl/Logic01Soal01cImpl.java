package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal01cImpl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal01cImpl(BasicLogic logic) {
        this.logic = logic;
    }

    void dalemanArray(){
        for (int i = 0; i < this.logic.n; i++) {

            String test = this.logic.array[0][i];
            this.logic.array[0][i] = String.valueOf(i+1);

        }
    }

    @Override
    public void cetakArray() {
        this.dalemanArray();
        this.logic.printSingle();
    }
}
