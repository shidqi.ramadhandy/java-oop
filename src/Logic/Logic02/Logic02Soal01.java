package Logic.Logic02;

import Logic.BasicLogic;

public class Logic02Soal01 extends BasicLogic {
    public Logic02Soal01(int n) {
        super(n);
    }

    public void isiArray(){

        for (int i = 0; i < this.n; i++) {
            int angka=1;
            // kolom
            for (int j = 0; j < this.n; j++) {
                if(i == j){
                    this.array[i][j] = String.valueOf(angka);
                }else if(i+j == n-1){
                    this.array[i][j] = String.valueOf(angka);
                }
                angka++;
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
