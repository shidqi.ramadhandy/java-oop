package Logic.Logic02;

import Logic.BasicLogic;

public class Logic02Soal09 extends BasicLogic {
    public Logic02Soal09(int n) {
        super(n);
    }

    public void isiArray() {
        int nilaiTampung = 0;
        int nilaiTengah = this.n / 2;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
//                if (j - i <= nilaiTengah && i - j <= nilaiTengah &&
//                        i + j >= nilaiTengah && i + j <= nilaiTengah + n - 1) {
//                    this.array[i][j] = "*";
//                }
                if (i == 0 && j == nilaiTengah || j == 0 && i == nilaiTengah) {
                    this.array[i][j] = String.valueOf(1);
                } else if (j == nilaiTengah && i <= nilaiTengah) {
                    this.array[i][j] = String.valueOf(Integer.parseInt(this.array[i - 1][j]) + 2);
                } else if (j == nilaiTengah && i > nilaiTengah) {
                    this.array[i][j] = String.valueOf(Integer.parseInt(this.array[i - 1][j]) - 2);
                }
            }
            if (i <= nilaiTengah && i > 0) {
                for (int j = 1; j <= i; j++) {
                    this.array[i][nilaiTengah + j] = String.valueOf(Integer.parseInt(this.array[i][nilaiTengah]) - 2 * j);
                    this.array[i][nilaiTengah - j] = String.valueOf(Integer.parseInt(this.array[i][nilaiTengah]) - 2 * j);
                }
                nilaiTampung++;
            } else {
                for (int j = 1; j <= nilaiTampung; j++) {
                    this.array[i][nilaiTengah + j] = String.valueOf(Integer.parseInt(this.array[i][nilaiTengah]) - 2 * j);
                    this.array[i][nilaiTengah - j] = String.valueOf(Integer.parseInt(this.array[i][nilaiTengah]) - 2 * j);
                }
                nilaiTampung--;
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.print();
    }

}
