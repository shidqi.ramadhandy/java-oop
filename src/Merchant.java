public class Merchant extends Person{
    public String itemForSale;
    public Integer merchantId;

    public Merchant(Integer id, String name, String gender, String address, Integer age, String itemForSale, Integer merchantId) {
        super(id, name, gender, address, age);
        this.itemForSale = itemForSale;
        this.merchantId = merchantId;
    }
}
