package variableHiding;

public class MainHiding {
    public static void main(String[] args) {
        Parent parent = new Parent();
        parent.name = "Juned";
        parent.doIt();
        System.out.println("Parent Name "+ parent.name);

        Child child = new Child();
        child.name = "Koko";
        child.doIt();
        System.out.println("Child Name "+ child.name);

        Parent parent2 = (Parent) child;
        parent2.doIt();
        System.out.println("Parent2 Name "+ parent2.name);

    }
}
