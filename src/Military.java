public class Military extends Person{

    public String force;
    public String grade;
    public Integer militaryId;

    public Military(Integer id, String name, String gender, String address, Integer age, String force, String grade, Integer militaryId) {
        super(id, name, gender, address, age);
        this.force = force;
        this.grade = grade;
        this.militaryId = militaryId;
    }
}
