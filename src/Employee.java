public class Employee extends Person{
    public String job;
    public Integer employeeId;


    public Employee(Integer id, String name, String gender, String address, Integer age, String job, Integer employeeId) {
        super(id, name, gender, address, age);
        this.job = job;
        this.employeeId = employeeId;
    }
}
