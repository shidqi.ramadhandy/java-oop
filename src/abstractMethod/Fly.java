package abstractMethod;

public class Fly extends Butterfly{
    public Fly(String name) {
        super(name);
    }

    @Override
    void green() {
        super.green();
        System.out.println("The Fly " + name + " color is green");
    }
}
