package abstractMethod;

public class Ladybug extends Bug{
    public Ladybug(String name){
        this.name = name;
    }

    @Override
    void green() {
        System.out.println("The Ladybug " + name + " color is green");
    }
}
