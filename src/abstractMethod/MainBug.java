package abstractMethod;

public class MainBug {
    public static void main(String[] args) {
        Butterfly butterfly = new Butterfly("Dodo");
        butterfly.green();
        Fly fly = new Fly("Messi");
        fly.green();
        Dragonfly dragonfly = new Dragonfly("Kane");
        dragonfly.green();
        Ladybug ladybug = new Ladybug("Jojo");
        ladybug.green();
        Beetle beetle = new Beetle("Blue Beetle");
        beetle.green();
        Ant ant = new Ant("Scott Lang");
        ant.green();
    }
}
