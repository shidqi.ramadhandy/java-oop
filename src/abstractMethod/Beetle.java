package abstractMethod;

public class Beetle extends Bug{
    public Beetle(String name){
        this.name = name;
    }

    @Override
    void green() {
        System.out.println("The Beetle " + name + " color is green");
    }
}
