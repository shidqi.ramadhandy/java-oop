package abstractMethod;

public class Dragonfly extends Bug{
    public Dragonfly (String name){
        this.name = name;
    }
    @Override
    void green() {
        System.out.println("The Dragonfly " + name + " color is green");
    }
}
