package abstractMethod;

public abstract class Bug {
    String name;

    abstract void green();
}
