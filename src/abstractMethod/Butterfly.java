package abstractMethod;

public class Butterfly extends Bug {

 public Butterfly(String name){ //costructor
     this.name = name;
 }

    @Override
    void green() {

        System.out.println("The Butterfly " + name + " color is green");
    }
}
