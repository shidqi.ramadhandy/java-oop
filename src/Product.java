public class Product {
    private Integer id;
    private String name;
    private Double price;


    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Product(Integer id, String name, Double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public void myProduct(){
        System.out.println("Nama : "+name+" Harga : "+price);
    }
}
