public class Teacher extends Person{
    public String major;
    public Integer teacherId;

    public Teacher(Integer id, String name, String gender, String address, Integer age, String major, Integer teacherId) {
        super(id, name, gender, address, age);
        this.major = major;
        this.teacherId = teacherId;
    }
}
