package Polymorphism;

public class MainPolymorphism {

    public static void main(String[] args) {
        Citizen citizen01 = new Citizen("Momonga");
        Guard guard01 = new Guard("Cocytus");
        Knight knight01 = new Knight("Shaltear Bloodfallen");


//    citizen01.sayHello();
//    knight01.sayHello();
//    guard01.sayHello();
        //cast
sayHello(citizen01);
sayHello(guard01);
sayHello(knight01);




    }
    //check & cast
    static void sayHello(Citizen citizen){
    if (citizen instanceof Guard){
        Guard guard = (Guard) citizen;
        System.out.println("Hello Guard " + guard.name);
    } else if (citizen instanceof Knight) {
        Knight knight = (Knight) citizen;
        System.out.println("Hello Knight " + knight.name);
    } else {
        System.out.println("Hello Citizen " + citizen.name);
    }
    }
}
