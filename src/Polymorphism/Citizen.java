package Polymorphism;

public class Citizen {
    String name;

     Citizen(String name) {
        this.name = name;
    }

void sayHello(){
    System.out.println("Hello Citizen " + this.name);
}
}
