public class Person {
    public Integer id;
    public String name;
    public String gender;
    public String address;

    public Integer age;

    //
    public void sayHello(){
        System.out.println("Hello "+name+" Selamat Datang");
        System.out.println("Umurmu "+age);
        System.out.println("Kamu Tinggal di " + address);

    }

    public Person() {
    }

    public Person(Integer id, String name, String gender, String address, Integer age) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.age = age;
    }
}
