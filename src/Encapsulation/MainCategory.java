package Encapsulation;

public class MainCategory {
    public static void main(String[] args) {
//        Category category = new Category();
//        category.setId(1);
//        category.setName("Botol Aqua");
//        category.setPrice(20000);
//        System.out.println(category);

        System.out.println(new Category(2, "Susu Kotak", 2500));
        System.out.println(new Category(3, "Teh Sisri", 1000));
    }
}
