package Interface;

public class HasBrandImpl implements HasBrand {
    private String brandName;

    private String name;

    private Integer price;

    private String brandType;


    public HasBrandImpl(String brandName, String name ,Integer price, String brandType) {
        this.brandName = brandName;
        this.name = name;
        this.price = price;
        this.brandType = brandType;
    }

    @Override
    public String getBrand() {
        return this.brandName;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getTypeBrand() {
        return this.brandType;
    }

    @Override
    public Integer getPrice() {
        return this.price;
    }
}
