package Interface;

public interface HasBrand {

    String getBrand();

    String getName();

    String getTypeBrand();

    Integer getPrice();

}
