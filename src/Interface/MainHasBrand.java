package Interface;

public class MainHasBrand {
    public static void main(String[] args) {
        HasBrand brand = new HasBrandImpl("Indomie", "Mie Goreng", 3500, "Makanan" );

        System.out.println("Brand Name "+ brand.getBrand());
        System.out.println("Brand Type Name " + brand.getName());
        System.out.println("Brand Type " + brand.getTypeBrand());
        System.out.println("Brand Price " + brand.getPrice());

    }
}
